import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'sample-angular-client';

  constructor() {
    console.log("Application is created");
    this.title = "client-app-modified"
  }

  ngAfterViewInit() {
    console.log("View is loaded");
  }
}
